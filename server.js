'use strict'

import { WebSocketServer } from 'ws';
import http from 'http';
import express from 'express';
import path from 'path';
import { fileURLToPath } from 'url';
import { v4 as uuidv4 } from 'uuid';
import paramValidate from './tool/param-validate.js';
import process from './controller/process.js';
import moment from 'moment';

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

let wss = null;

/**
 * ws connection 方法
 * @param {object} ws ws物件
 * 
 */
function connection(ws) {
  ws.on('message', onMessage);
  ws.on('close', onClose);

  connectionInitialization(ws);
  process.choeseChatroom(ws);
};

/**
 * ws message 方法
 * @param {object} message 接收訊息物件
 * 
 */
function onMessage(message) {
  try {
    message = JSON.parse(message);
  } catch (error) {
    console.error(`message json parse error: `, error);
    return this.processResponseData(this, 'onMessage', {}, '0003');
  }
  
  const validateResult = paramValidate.onMessage(message);

  if (!validateResult) {
    return this.processResponseData(this, 'onMessage', {}, '0001');
  }

  switch(message.event) {
    case 'joinRoom':
      process.joinRoom(wss, this, message.data);
      break;
    case 'speak':
      process.speak(wss, this, message.data);
      break;
    case 'leave':
      process.leave(wss, this);
      break;
    default:
      console.log(`default: `, message);
      return this.processResponseData(this, 'onMessage', {}, '0004');
  }
};

/**
 * ws close 方法
 * 
 */
function onClose() {
  let exclude = {
    id: this.id,
    location: this.location
  }
  let timestamp = moment().format('X');
  process.receiveLeave(wss, this.username, timestamp, exclude);
};

/**
 * 處理 ws send 回應資料方法
 * @param {object} ws ws物件
 * @param {string} event 事件名稱
 * @param {object} data 回應資料物件
 * @param {string} code 狀態碼
 * 
 */
function processResponseData(ws, event, data, code = '0000') {
  let response = {
    event,
    code,
    data
  }
  ws.send(JSON.stringify(response));
};

/**
 * 連線初始化方法
 * @param {object} ws ws物件
 * 
 */
function connectionInitialization(ws) {
  ws.id = uuidv4();
  ws.location = 'lobby';
  ws.username = '';
  ws.processResponseData = processResponseData;
};

/**
 * 主程序
 * 
 */
function main() {
  const app = express();
  const server = http.createServer(app)
  server.listen(8080, () => {
    console.log('server start')
  });
  wss = new WebSocketServer({server});
  
  wss.on('connection', connection);

  const router = express.Router();
  app.use(router);

  app.use('/public', express.static(__dirname + '/public'));

  app.use('/index.html', express.static(__dirname + '/views/index.html'));

};

main();

