'use strict'

import WebSocket from 'ws';
import paramValidate from '../tool/param-validate.js';
import moment from 'moment';

// 聊天室物件
const chatroom = {
  "basketball": "籃球",
  "baseball": "棒球",
  "football": "足球",
  "tennis": "網球"
};

export  default {
  /**
   * 處理 choeseChatroom (選擇聊天室) 流程方法
   * @param {object} ws ws物件
   * 
   */
  choeseChatroom(ws) {
    const event = 'choeseChatroom';
    let code = '0000';
    let response = {
      chatroom: []
    };
    for (let key in chatroom) {
      let object = {
        name: chatroom[key],
        value: key
      };
      response.chatroom.push(object);
    }
    ws.processResponseData(ws, event, response, code);
    return;
  },
  /**
   * 處理 joinRoom 流程方法
   * @param {object} wss ws server物件
   * @param {object} ws ws物件
   * @param {object} data 請求資料物件
   * 
   */
  joinRoom(wss, ws, data) {
    const event = 'joinRoom';
    let code = '0000';
    let response = {
      chatroom: '',
      username: '',
      timestamp: 0
    };

    const validateResult = paramValidate.joinRoom(data, Object.keys(chatroom));

    if (!validateResult) {
      return ws.processResponseData(ws, event, {}, '0001');
    }
    if (ws.location == data.chatroom) {
      return ws.processResponseData(ws, event, {}, '0002');
    }

    ws.location = data.chatroom;
    ws.username = data.username;
    response.chatroom = ws.location;
    response.username = ws.username;
    response.timestamp = moment().format('X');

    let filter = {
      id: ws.id,
      location: ws.location
    }
    ws.processResponseData(ws, event, response, code);
    this.receiveJoinRoom(wss, ws.username, response.timestamp, filter);
    return;
  },
  /**
   * 處理 speak (發送對話) 流程方法
   * @param {object} wss ws server物件
   * @param {object} ws ws物件
   * @param {object} data 請求資料物件
   * 
   */
  speak(wss, ws, data) {
    const event = 'speak';
    let code = '0000';
    let response = {
      content: '',
      timestamp: 0
    };

    const validateResult = paramValidate.speak(data);

    if (!validateResult) {
      return ws.processResponseData(ws, event, {}, '0001');
    }

    if (ws.location == 'lobby') {
      return ws.processResponseData(ws, event, {}, '0005');
    }

    response.content = data.content;
    response.timestamp = moment().format('X');

    ws.processResponseData(ws, event, response, code);

    let filter = {
      id: ws.id,
      location: ws.location
    };

    this.receiveSpeak(wss, ws.username, data.content, response.timestamp, filter);
    return;
  },
  /**
   * 處理 leave (離開聊天室) 流程方法
   * @param {object} wss ws server物件
   * @param {object} ws ws物件
   * 
   */
  leave(wss, ws) {
    const event = 'leave';
    let code = '0000';
    let response = {
      leaver: '',
      timestamp: 0
    };

    if (ws.location == 'lobby') {
      return ws.processResponseData(ws, event, {}, '0005');
    }

    response.leaver = ws.username;
    response.timestamp = moment().format('X');

    let filter = {
      id: ws.id,
      location: ws.location
    }
    ws.location = 'lobby';
    ws.username = '';
    ws.processResponseData(ws, event, response, code);
    this.receiveLeave(wss, response.leaver, response.timestamp, filter);
    return;
  },
  /**
   * 處理 receiveJoinRoom (接收其他人進聊天室) 流程方法
   * @param {object} wss ws server物件
   * @param {string} joiner 加入人
   * @param {number} timestamp 加入聊天室時間戳記，單位秒
   * @param {object} filter 篩選資料物件
   * 
   */
  receiveJoinRoom(wss, joiner, timestamp, filter) {
    const event = 'receiveJoinRoom';
    const response = {
      joiner,
      timestamp
    };

    wss.clients.forEach(function each(client) {
      if (client.readyState != WebSocket.OPEN) return;

      if (client.id == filter.id) return;

      if (client.location != filter.location) return;

      client.processResponseData(client, event, response, '0000');
    });
  },
  /**
   * 處理 receiveSpeak (接收對話) 流程方法
   * @param {object} wss ws server物件
   * @param {string} sender 發訊人
   * @param {string} content 發送訊息內容
   * @param {number} timestamp 發送訊息時間戳記，單位秒
   * @param {object} filter 篩選資料物件
   * 
   */
  receiveSpeak(wss, sender, content, timestamp, filter) {
    const event = 'receiveSpeak';
    const response = {
      sender,
      content,
      timestamp
    };

    wss.clients.forEach(function each(client) {
      if (client.readyState != WebSocket.OPEN) return;

      if (client.id == filter.id) return;

      if (client.location != filter.location) return;

      client.processResponseData(client, event, response, '0000');
    });
  },
  /**
   * 處理 receiveLeave (接收離開訊息) 流程方法
   * @param {object} wss ws server物件
   * @param {string} leaver 離開人
   * @param {number} timestamp 離開聊天室時間戳記，單位秒
   * @param {object} filter 篩選資料物件
   * 
   */
  receiveLeave(wss, leaver, timestamp, filter) {
    const event = 'receiveLeave';
    const response = {
      leaver,
      timestamp
    };

    wss.clients.forEach(function each(client) {
      if (client.readyState != WebSocket.OPEN) return;

      if (client.id == filter.id) return;

      if (client.location != filter.location) return;

      client.processResponseData(client, event, response, '0000');
    });
  }
};