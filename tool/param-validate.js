'use strict'

import joi from 'joi';

export  default {
  /**
   * 驗證 onMessage 資料參數
   * @param {object} data 請求資料物件
   * 
   * @returns {boolean} 結果；驗證成功：true、驗證失敗：false
   */
  onMessage(data) {

    const scheme = joi.object({
      event: joi.string().required(),
      data: joi.object().required()
    });

    const { error} = scheme.validate(data);

    if (error != undefined) {
      console.log(`onMessage validate error:`, error);
      return false;
    }
    return true;
  },
  /**
   * 驗證 joinRoom 資料參數
   * @param {object} data 請求資料物件
   * @param {array} chatroomCodeArray 聊天室英文代號陣列
   * 
   * @returns {boolean} 結果；驗證成功：true、驗證失敗：false
   */
  joinRoom(data, chatroomCodeArray) {

    const scheme = joi.object({
      chatroom: joi.string()
                      .valid(...chatroomCodeArray)
                      .required(),
      username: joi.string()
                  .min(1)
                  .max(20)
                  .required()
    });

    const { error} = scheme.validate(data);

    if (error != undefined) {
      console.log(`joinRoom validate error:`, error);
      return false;
    }
    return true;
  },
  /**
   * 驗證 speak 資料參數
   * @param {object} data 請求資料物件
   * 
   * @returns {boolean} 結果；驗證成功：true、驗證失敗：false
   */
  speak(data) {

    const scheme = joi.object({
      content: joi.string()
                  .min(1)
                  .max(100)
                  .required()
    });

    const { error} = scheme.validate(data);

    if (error != undefined) {
      console.log(`speak validate error:`, error);
      return false;
    }
    return true;
  }
};