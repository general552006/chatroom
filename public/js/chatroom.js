const {ref, nextTick, onMounted, onUpdated, createApp } = Vue
moment.suppressDeprecationWarnings = true

const vm = createApp({
  setup() {
    let socket = null
    let isJoinChatroom = ref(false)
    let chatroom = ref([])
    let inputName = ref('')
    let selectChatroom = ref('')
    let massageList = ref([])
    let inputContent = ref('')
    let isShowErrorTip = ref(false)
    let errorMessage = ref('')
    let listDiv = ref(null)

    onMounted(() => {
      connectWebSocket()
    })

    onUpdated(() => {
    })

    /**
     * WebSocket 連線方法
     */
    function connectWebSocket() {  

      socket = new WebSocket('ws://localhost:8080')
    
      socket.onopen = function() {
        console.log('websocket connected!!')
      }

      socket.onclose = async function() {
        console.log('websocket close!!')
        showErrorTip()
        backToDefault()
      }
      socket.onmessage = async function(message) {

        let response = JSON.parse(message.data)

        if (response.code != '0000') {
          showErrorTip(response.code)
          console.error(`websocket response error: `, response)
          return
        }

        switch(response.event) {
          case 'choeseChatroom':
            await nextTick()
            chatroom.value = response.data.chatroom
            break
          case 'joinRoom':
            await nextTick()
            isJoinChatroom.value = true
            let joinRoomObject = {
              isMe: true,
              sender: inputName.value,
              content: `【我】進入聊天室`,
              time: moment.unix(response.data.timestamp).format('YYYY-MM-DD HH:mm:ss')
            }
            massageList.value.push(joinRoomObject)
            break
          case 'receiveJoinRoom':
            await nextTick()
            let receiveJoinRoomObject = {
              isMe: false,
              sender: response.data.joiner,
              content: `【${response.data.joiner}】進入聊天室`,
              time: moment.unix(response.data.timestamp).format('YYYY-MM-DD HH:mm:ss')
            }
            massageList.value.push(receiveJoinRoomObject)
            scrollBottom()
            break
          case 'speak':
            await nextTick()
            let speakObject = {
              isMe: true,
              sender: inputName.value,
              content: `【我】發言： ${response.data.content}`,
              time: moment.unix(response.data.timestamp).format('YYYY-MM-DD HH:mm:ss')
            }
            massageList.value.push(speakObject)
            inputContent.value = ''
            scrollBottom()
            break
          case 'receiveSpeak':
            await nextTick()
            let receiveSpeakObject = {
              isMe: false,
              sender: response.data.sender,
              content: `【${response.data.sender}】發言： ${response.data.content}`,
              time: moment.unix(response.data.timestamp).format('YYYY-MM-DD HH:mm:ss')
            }
            massageList.value.push(receiveSpeakObject)
            scrollBottom()
            break
          case 'leave':
            backToDefault()
            break
          case 'receiveLeave':
            await nextTick()
            let receiveLeaveObject = {
              isMe: false,
              sender: response.data.leaver,
              content: `【${response.data.leaver}】離開聊天室`,
              time: moment.unix(response.data.timestamp).format('YYYY-MM-DD HH:mm:ss')
            }
            massageList.value.push(receiveLeaveObject)
            scrollBottom()
            break
          default:
            break
        }
      }
      socket.onerror = function(error) {
        console.log('websocket error: ', error)
      }
      socket.sendProcess = function(event, data) {
        if (this.readyState === 1) {
          let message = {
            event,
            data 
          }
          this.send(JSON.stringify(message))
        } else {
          showErrorTip()
        }
      }
    }

    /**
     * 發送 WebSocket joinRoom 事件方法
     */
    function sendJoinRoom() {
      if (isJoinChatroom.value) {
        return
      }
      if (selectChatroom.value === '' || inputName.value === '') {
        return
      }
      let data = {
        chatroom: selectChatroom.value,
        username: inputName.value
      }
      socket.sendProcess('joinRoom', data)
    }

    /**
     * 發送 WebSocket speak 事件方法
     */
    function sendSpeak() {
      if (!isJoinChatroom.value) {
        return
      }
      if (inputContent.value == '') {
        return
      }
      let data = {
        content: inputContent.value
      }
      socket.sendProcess('speak', data)
    }

    /**
     * 發送 WebSocket leave 事件方法
     */
    function sendLeaveRoom() {
      if (!isJoinChatroom.value) {
        return
      }
      let data = {
      }
      socket.sendProcess('leave', data)
    }

    /**
     * 顯示錯誤訊息提示窗方法
     */
    function showErrorTip(code) {
      if (isShowErrorTip.value) {
        return
      }
      switch(code) {
        case '0001':
          errorMessage.value = '參數不符合規定'
          break
        case '0002':
          errorMessage.value = '進入相同類型聊天室'
          break
        case '0003':
          errorMessage.value = '物件變數解析失敗'
          break
        case '0004':
          errorMessage.value = '使用不存在的接口'
          break
        case '0005':
          errorMessage.value = '在大廳無法使用該接口'
        default:
          errorMessage.value = 'WebSocket中斷'
          break
      }
      isShowErrorTip.value = true
      setTimeout(() => {
        isShowErrorTip.value = false
        errorMessage.value = ''
      }, 1000)
    }

    /**
     * 回到預設值方法
     */
    async function backToDefault() {
      await nextTick()
      isJoinChatroom.value = false
      inputName.value = ''
      selectChatroom.value = ''
      massageList.value = []
      inputContent.value = ''
    }

    /**
     * 捲軸置底方法
     */
    async function scrollBottom() {
      await nextTick()
      listDiv.value.scrollTop = listDiv.value.scrollHeight
    }

    return {
      connectWebSocket,
      sendJoinRoom,
      sendSpeak,
      sendLeaveRoom,
      showErrorTip,
      scrollBottom,
      isJoinChatroom,
      chatroom,
      selectChatroom,
      inputName,
      massageList,
      inputContent,
      isShowErrorTip,
      errorMessage,
      listDiv
    }
  }
})

vm.mount('#app')

