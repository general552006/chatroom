# 多類型頻道聊天室

## 功能介紹
- 可選擇不同聊天室，進行多人聊天對話

<br>

## 執行方式

- 使用終端機
```txt

$ git clone https://gitlab.com/general552006/chatroom.git

$ cd chatroom

$ npm run start

```
- 開啟瀏覽器
  - 輸入網址：[http://localhost:8080/index.html](http://localhost:8080/index.html)
- 示意畫面
  - ![示意畫面](docs/demo.gif)


<br>

## 狀態碼說明

|  代碼     |   說明           |
| -------- | ---------------- |
| 0000     | 成功              |
| 0001     | 參數不符合規定      |
| 0002     | 進入相同類型聊天室  | 
| 0003     | 物件變數解析失敗    | 
| 0004     | 使用不存在的接口    | 
| 0005     | 在大廳無法使用該接口 | 
